from django.conf.urls import url
from . import views

urlpatterns=[
	url(r'^$',views.index,name="index"),
	url(r'^author/(?P<author_slug>[\w\-]+)/$',views.author,name="author"),
	url(r'^search/$',views.search,name="search"),
	url(r'^newsletter/$',views.newsletter,name="newsletter"),
	url(r'^tag/(?P<slug>[\w\-]+)/$',views.tag,name="tag"),
	#url(r'^dashboard/$',views.DashboardListView.as_view(),name="dashboard"),
	

	#url(r'^register/$',views.register,name="register"),
	#url(r'^login/$',views.user_login,name="login"),
	#url(r'^logout/$',views.user_logout,name="logout"),
	
	url(r'^ajax/add_comment/$',views.add_comment,name="add_comment"),
	url(r'^ajax/suggest/$',views.suggest,name="suggest"),
	url(r'^ajax/all/$',views.all,name="all"),


	url(r'^(?P<slug>[\w\-]+)/$',views.single,name="single"),
]