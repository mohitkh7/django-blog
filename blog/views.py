from django.shortcuts import render,get_object_or_404,redirect
from django.http import HttpResponse,HttpResponseRedirect
from django.contrib.auth import authenticate,login,logout
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator,PageNotAnInteger,EmptyPage
from django.views.decorators.csrf import ensure_csrf_cookie
from django.utils import timezone
from django.http import JsonResponse
from django.views import generic

from django_elasticsearch.models import EsIndexable
from taggit.models import Tag

from .models import Blog,Newsletter,Author,Comment
from .forms import BlogForm

def convert(esquery):
	b=[]
	for i in esquery:
		ids=i['id']
		print(ids)
		b+=Blog.objects.filter(id=ids)
		print (b)
	return b

class TagMixin(object):
    def get_context_data(self, kwargs):
        context = super(TagMixin, self).get_context_data(kwargs)
        context['tags'] = Tag.objects.all()
        return context

# Create your views here.
def index(request):
	#return HttpResponse("Jai Shree Ram")
	blog_list=Blog.objects.filter(is_published=True).order_by('-created_on')

	#This is for Pagination 
	no_of_post=5	#No of posts to be shown onn one page
	paginator = Paginator(blog_list, no_of_post)
	page=request.GET.get('page')
	try:
		blogs=paginator.page(page)
	except PageNotAnInteger:
		blogs=paginator.page(1)
	except EmptyPage:
		blogs=paginator.page(paginator.num_pages)   

	#popular=Blog.objects.all().order_by('-views')[0:5]
	return render(request,'blog/index.html',{'blogs':blogs,})

def single(request,slug):
	#for single blog
	blog=get_object_or_404(Blog,slug=slug)
	blog.views+=1
	blog.save()
	comments=Comment.objects.filter(blog=blog).order_by('-datetime')
	#for similar blogs
	#similar=Blog.objects.filter(is_published=True)[:2]
	similar=blog.tags.similar_objects()
	#if there is no similar post by tag method then
	if not similar:
		similar=Blog.objects.filter(is_published=True).exclude(id=blog.id).order_by('?')[:2]
	'''if len(similar)==1:
		similar+=Blog.objects.filter(is_published=True).exclude(id=blog.id).exclude(id=similar[0].id).order_by('?')[:1]'''
	return render(request,'blog/single.html',{'blog':blog,'similar':similar,'comments':comments,'c':similar.count})

#For search result of side right bar
def search(request):
	if request.method=="GET":
		query=request.GET.get('search')
		#blog=Blog.objects.filter(content__contains=query).filter(is_published=True)
		blog=Blog.es.search(query)
		blog=convert(blog)
		#blog=blog.d
		return render(request,'blog/search.html',{'query':query,'blogs':blog})

#To show all the post related to a tag
def tag(request,slug):
	#return HttpResponse("bam"+slug)
	blog=Blog.objects.filter(tags__slug__in=[slug])
	return render(request,'blog/search.html',{'query':slug,'blogs':blog})

#For Newsletter signup
def newsletter(request):
	if request.method=="POST":
		name=request.POST.get('name')
		email=request.POST.get('email')
		n=Newsletter(name=name,email=email)
		n.save()
	return HttpResponseRedirect("/blog/")

#to display details of author
def author(request,author_slug):
	author=get_object_or_404(Author,slug=author_slug)
	blogs=Blog.objects.filter(author=author).filter(is_published=True)
	return render(request,'blog/author.html',{'author':author,'blogs':blogs})

#Author DashBoard System - CRUD on Blog

class DashboardListView(generic.ListView):
	model=Author

def suggest(request):
	if request.method=='GET':
		if request.is_ajax():
			q=request.GET.get('query')
			print("Query"+q)
			sug=Blog.es.complete('title',q)
			sug=Blog.es.all()
			print(sug)
			return JsonResponse({'msg':'do','sug':sug})

def all(request):
	if request.method=='GET':
		if request.is_ajax():
			B=Blog.objects.values_list('title')
			B=list(B)
			print(type(B))
			print(B)
			return JsonResponse({'msg':'do','blog':B})

def add_comment(request):
	if request.method == 'POST':
		#POST goes here . is_ajax is must to capture ajax requests. Beginner's pit.
		if request.is_ajax():
			comment=request.POST.get('comment')
			pk=request.POST.get('blogPK')
			blog=Blog.objects.get(pk=pk)
			user=request.user
			datetime=timezone.now()
			#blog=request.POST.get('blog')

			c=Comment(comment=comment,user=user,datetime=datetime,blog=blog)
			c.save()
			
			'''
			name=request.POST.get('comment')
			email=request.POST.get('csrfmiddlewaretoken')
			n=Newsletter(name=name,email=email)
			n.save()
			'''
			return JsonResponse({'msg':'suck'})
