from django.contrib import admin
from .models import Blog,Author,Newsletter,Comment

#Further Classes
class NewsletterAdmin(admin.ModelAdmin):
	list_display=('name','email')

class BlogAdmin(admin.ModelAdmin):
	list_display=('title','views','slug')

class CommentAdmin(admin.ModelAdmin):
	list_display=('blog','short_comment','user','datetime')
	ordering=['datetime','blog','user']
	list_filter=('blog','user')

# Register your models here.

admin.site.register(Blog,BlogAdmin)
admin.site.register(Author)
#admin.site.register(Tag)
admin.site.register(Comment,CommentAdmin)
admin.site.register(Newsletter,NewsletterAdmin)