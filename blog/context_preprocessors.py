from .models import Blog
from taggit.models import Tag

def context(request):
	popular=Blog.objects.filter(is_published=True).order_by('-views')[:5]
	tagcloud=Tag.objects.all()
	sizes=[1,1.2,1.4,1.6,1.8,2]
	return {'popular':popular,'tagcloud':tagcloud,'sizes':sizes}