from django import forms
from .models import Blog

from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm

class BlogForm(forms.ModelForm):
	class Meta:
		model=Blog
		exclude=('views','slug','created_on','author')

class PictureWidget(forms.widgets.Widget):
	
    def render(self, name, value, attrs=None):
        html = Template("""<img src="$link"/>""")
        return mark_safe(html.substitute(link=value))

class SignupForm(UserCreationForm):
	class Meta:
		model = User
		fields = ('username','first_name', 'last_name', 'email','password1','password2')

