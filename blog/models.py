from django.db import models
from django.template.defaultfilters import slugify
from django.utils import timezone

from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

from django.template.defaultfilters import truncatechars  # or truncatewords

from taggit.managers import TaggableManager #For Tag

from django_elasticsearch.models import EsIndexable #For indexing and search

# Create your models here.

class Blog(EsIndexable ,models.Model):
	title=models.CharField(max_length=300,unique=True,help_text="Write Specific Title")
	image=models.ImageField(upload_to="blog/",default="img.jpg")
	content=models.TextField()
	author=models.ForeignKey('author',on_delete=models.SET_NULL,null=True)
	created_on=models.DateTimeField(default=timezone.now)
	views=models.IntegerField(default=0)
	slug=models.SlugField(default="",blank=True)
	is_published=models.BooleanField("Publish the Blog ",default=True)
	tags=TaggableManager()

	#For indexing
	class Elasticsearch(EsIndexable.Elasticsearch):
		fields=['id','title','content','author']
		completion_fields=['title']

	#To generate slug
	def save(self,*args,**kwargs):
		self.slug=slugify(self.title)
		super(Blog,self).save(*args,**kwargs)

	#to display 
	#def __str__(self):
	#	return self.title

'''class Author(models.Model):
	name=models.CharField(max_length=100)
	def __str__(self):
		return self.name
'''

class Author(models.Model):
	user=models.OneToOneField(User,on_delete=models.CASCADE,null=True)
	name=models.CharField(max_length=100,unique=True)
	slug=models.SlugField(default="",blank=True)

	#for url
	def save(self,*args,**kwargs):
		self.slug=slugify(self.name)
		super(Author,self).save()


	#to display
	def __str__(self):
		return self.name

	@receiver(post_save, sender=User)
	def create_user_profile(sender, instance, created, **kwargs):
		if created:
			Author.objects.create(user=instance,name=instance.username)

	@receiver(post_save, sender=User)
	def save_user_profile(sender, instance, **kwargs):
		instance.author.save()


class Newsletter(models.Model):
	name=models.CharField(max_length=200)
	email=models.EmailField()

	def __str__(self):
		return self.name

class Comment(models.Model):
	comment=models.TextField()
	#user=models.ForeignKey('User',on_delete=models.CASCADE)
	user=models.CharField(max_length=200)
	blog=models.ForeignKey('Blog',on_delete=models.CASCADE)
	datetime=models.DateTimeField(default=timezone.now)

	@property
	def short_comment(self):
		return truncatechars(self.comment,30)

	def __str__(self):
		com=truncatechars(self.comment,30)
		return com