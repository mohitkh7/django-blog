from django.shortcuts import render,get_object_or_404,redirect
from django.http import HttpResponse,HttpResponseRedirect
from django.contrib.auth import authenticate,login,logout
from django.contrib.auth.decorators import login_required
from django.views import generic
from django.core.paginator import Paginator,PageNotAnInteger,EmptyPage
from django.utils import timezone

from blog.models import Blog,Author
from blog.forms import BlogForm,SignupForm

# Create your views here.
@login_required
def index(request):
	blogs=Blog.objects.filter(is_published=True).order_by('-created_on')
	drafts=Blog.objects.filter(is_published=False).order_by('-created_on')
	return render(request,"dashboard/index.html",{'blogs':blogs,'drafts':drafts})
	#return HttpResponse("ko")

#to add a new blog
@login_required
def add(request):
	blogs=Blog.objects.all().order_by('created_on')
	if request.method=="POST":
		form=BlogForm(request.POST, request.FILES)
		if form.is_valid():
			blog=form.save(commit=False)
			blog.created_on=timezone.now()
			author=Author.objects.get(user=request.user)
			blog.author=author
			blog.save()
			#To save blog we will do this
			form.save_m2m()
			#return HttpResponseRedirect("/blog/")
			return redirect("index")
	
	else:
		form=BlogForm()

	return render(request,'dashboard/add.html',{'blogs':blogs,'form':form})

#Delete a Blog
def delete(request,id):
	blog=get_object_or_404(Blog,pk=id)
	blog.delete()
	return redirect('/dashboard/');

#Edit A blog
def edit(request,id):
	blog=get_object_or_404(Blog,pk=id)
	if request.method=="POST":
		form=BlogForm(request.POST, request.FILES,instance=blog)
		if form.is_valid():
			#b=form.save()
			#blog.update(b)
			#Blog.objects.get(pk=id).update(form.save())
			#blog.created_on=timezone.now()
			#return HttpResponseRedirect("/blog/")
			#blog=form
			blog=form.save(commit=False)
			blog.save()
			#For tags
			form.save_m2m()
			return redirect("dashboard")
	
	else:
		form=BlogForm(instance=blog)

	return render(request,'dashboard/edit.html',{'blog':blog,'form':form})


class BlogEdit(generic.UpdateView):
	model=Blog
	
'''
Account Related activities begins here onwards
'''
def signup(request):
	#if user is alrady logged in
	if request.user.is_authenticated():
		return redirect('dashboard')

	#if form is submitted register button
	if request.method=="POST":
		form=SignupForm(request.POST)
		if form.is_valid():
			form.save()#Account Created
			#Signing up
			username=form.cleaned_data.get("username")
			password=form.cleaned_data.get("password1")
			user=authenticate(username=username,password=password)
			login(request,user)
			return redirect('dashboard')
	else:
		form=SignupForm()
	return render(request,"dashboard/signup.html",{'form':form})

def user_login(request):
	if request.user.is_authenticated():
		return redirect('dashboard')

	if request.method=="POST":
		username=request.POST.get('username')
		password=request.POST.get('password')
		#Using Django authentication method
		user=authenticate(username=username,password=password)
		if user:
			login(request,user)
			next=request.GET.get('next')
			if next:
				return HttpResponseRedirect(request.GET.get('next'))
			else:
				return HttpResponseRedirect('/dashboard/')
		else:
			return render(request,'dashboard/login.html',{'error':"Invalid Username or Password"})

	return render(request,'dashboard/login.html')

def user_logout(request):
	logout(request)
	return HttpResponseRedirect('/blog/')