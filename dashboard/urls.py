from django.conf.urls import url
from . import views

urlpatterns=[
	url(r'^$',views.index,name="dashboard"),
	#url(r'^edit/(?P<id>[\w\-]+)/$',views.BlogEdit.as_view(),name="edit"),
	url(r'^add/$',views.add,name="add"),
	url(r'^edit/(?P<id>[\w\-]+)/$',views.edit,name="edit"),
	url(r'^delete/(?P<id>[\w\-]+)/$',views.delete,name="delete"),
	url(r'^signup/$',views.signup,name="signup"),
	url(r'^login/$',views.user_login,name="login"),
	url(r'^logout/$',views.user_logout,name="logout"),
]